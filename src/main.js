import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import HighchartsVue from 'highcharts-vue'

/**
 * -----------------------------------------
 *  Import Libraries for project
 * -----------------------------------------
 */
import './plugins/veeValidate'
import './plugins/vueCookies'
import './plugins/vueJsModal'
import './plugins/vueMoment'
import './plugins/vueDatePicker'

/**
 * -----------------------------------------
 *  Import Directives for project
 * -----------------------------------------
 */
import './directives/vueClickOutside'
Vue.use(HighchartsVue)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
