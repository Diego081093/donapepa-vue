// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_SALES: null,
  S_SALES_BY_PRODUCT: null,
  S_SALES_BY_LOCAL: null,
  S_SALES_BY_MONTH: null
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_SALES ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`reporte/ventas?idUsuario=${Params.idUsuario}&idProducto=${Params.idProducto}&ini=${Params.fechaIni}&fin=${Params.fechaFin}`)
      // console.log('jwt decode', jwtDecode(commit))
      commit('SET_DATA', {
        destination: 'S_SALES',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SALES [ACTION]', error)
      throw error
    }
  },
  async A_GET_SALES_BY_PRODUCT ({ commit }, Params) {
    try {
      const { data } = await HTTP.get('reporte/ventas/byProduct')
      commit('SET_DATA', {
        destination: 'S_SALES_BY_PRODUCT',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SALES [ACTION]', error)
      throw error
    }
  },
  async A_GET_SALES_BY_LOCAL ({ commit }, Params) {
    try {
      const { data } = await HTTP.get('reporte/ventas/byLocal')
      commit('SET_DATA', {
        destination: 'S_SALES_BY_LOCAL',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SALES [ACTION]', error)
      throw error
    }
  },
  async A_GET_SALES_BY_MONTH ({ commit }, Params) {
    try {
      const { data } = await HTTP.get('reporte/ventas/byMonth')
      commit('SET_DATA', {
        destination: 'S_SALES_BY_MONTH',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_SALES [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
