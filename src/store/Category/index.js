// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_CATEGORIES: [],
  S_CATEGORIE: {}
}
const getters = {
  G_CATEGORIES: (state) => {
    const categories = []
    state.S_CATEGORIES.forEach(element => {
      categories.push(
        {
          name: element.nombre,
          value: element.idCategoria + ''
        }
      )
    })
    if (categories.length === 0) {
      categories.push({ name: 'Seleccione', value: '0' })
    }
    return categories
  },
  G_BRANDS: (state) => {
    const brands = []
    state.S_BRANDS.forEach(element => {
      brands.push(
        {
          name: element.descripcion,
          value: element.descripcion
        }
      )
    })
    if (brands.length === 0) {
      brands.push({ name: 'Seleccione', value: '0' })
    }
    return brands
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idCategoria === params.data)
    state[params.destination].splice(index, 1)
    Vue.put()
    console.log('M_DELETE_CATEGORY', index)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idCategoria === params.data.idCategoria)
    state.S_CATEGORIES[index] = params.data
  }
}
const actions = {
  async A_GET_CATEGORIES ({ commit }, params) {
    try {
      const { data } = await HTTP.get('categoria/listar', { params })
      commit('SET_DATA', {
        destination: 'S_CATEGORIES',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_CATEGORIES [ACTION]', error)
      throw error
    }
  },
  async A_SET_CATEGORIE ({ commit }, category) {
    try {
      const { data } = await HTTP.post('categoria/agregar', category)
      console.log('data', data)
      if (data.status) {
        category.categoria.idCategoria = data.data
        category.categoria.habilitado = 1
        commit('PUSH_DATA', {
          destination: 'S_CATEGORIES',
          data: category.categoria
        })
      }
      return data
    } catch (error) {
      console.log('ERROR_SET_CATEGORIE [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_CATEGORY ({ commit }, id) {
    try {
      const { data } = await HTTP.put('categoria/eliminar', { categoria: { idCategoria: id } })

      commit('DELETE_DATA', {
        destination: 'S_CATEGORIES',
        data: id
      })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_DESACTIVE_CATEGORY ({ commit }, id) {
    try {
      const { data } = await HTTP.put('desactivar', { desactivar: { nombreTabla: 'Categoria', id: id } })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_ACTIVE_CATEGORY ({ commit }, id) {
    try {
      const { data } = await HTTP.put('activar', { activar: { nombreTabla: 'Categoria', id: id } })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_UPDATE_CATEGORIE ({ commit }, category) {
    try {
      const { data } = await HTTP.put('categoria/actualizar', category)
      category.categoria.data = {
        ...data
      }
      return category.categoria
    } catch (error) {
      console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
