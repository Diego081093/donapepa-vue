// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_DEPARTMENTS: []
}
const getters = {
  G_DEPARTMENTS: (state) => {
    const departments = []
    state.S_DEPARTMENTS.forEach(element => {
      departments.push(
        {
          name: element.nombre,
          value: element.idDepartamento + '',
          code: element.codigo
        }
      )
    })
    if (departments.length === 0) {
      departments.push({ name: 'Sin data', value: '0' })
    }
    return departments
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_DEPARTMENTS ({ commit }) {
    try {
      const { data } = await HTTP.get('departamento/listar')

      commit('SET_DATA', {
        destination: 'S_DEPARTMENTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_DEPARTMENTS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
