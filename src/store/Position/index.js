// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_POSITIONS: []
}
const getters = {
  G_POSITIONS: (state) => {
    const positions = [{ name: 'Selecciona el Cargo', value: '' }]
    state.S_POSITIONS.forEach(element => {
      positions.push(
        {
          name: element.nombre,
          value: element.idCargo + ''
        }
      )
    })
    return positions
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_POSITIONS ({ commit }) {
    try {
      const { data } = await HTTP.get('cargo/listar')

      commit('SET_DATA', {
        destination: 'S_POSITIONS',
        data: data
      })
    } catch (error) {
      console.log('ERROR_GET_POSITIONS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
