import Vue from 'vue'
import { HTTP } from '@/plugins/axios'

const state = {
  S_CUTS: []
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destinations, params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idCorte === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_CUT', index)
  }
}
const actions = {
  async A_GET_CUTS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('corte', { params })
      commit('SET_DATA', {
        destinations: 'S_CUTS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_CUT:', error)
      throw error
    }
  },
  async A_GET_TYPECUTS ({ commit }, idTipoTablero = null) {
    try {
      const get = (idTipoTablero) ? `?idTipoTablero=${idTipoTablero}` : ''
      const { data } = await HTTP.get(`tipocorte/listar${get}`)
      commit('SET_DATA', {
        destinations: 'S_CUTS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_CUT:', error)
      throw error
    }
  },
  async A_GET_TYPECUTDISTRIBUTIONS ({ commit }, idTipoCorte) {
    try {
      const { data } = await HTTP.get(`tipocortedistribucion/listar/${idTipoCorte}`)
      return data
    } catch (error) {
      console.error('ERROR_GET_BOARD', error)
      throw error
    }
  },
  async A_SET_CUT ({ commit }, corte) {
    try {
      const { data } = await HTTP.post('corte/agregar', { corte })
      return data
    } catch (error) {
      console.erro('ERROR_SET_CUT:', error)
      throw error
    }
  },
  async A_DELETE_CUT ({ commit }, idCorte) {
    try {
      const { data } = await HTTP.put('corte/eliminar', { corte: { idCorte } })
      commit('DELETE_DATA', {
        destination: 'S_CUTS',
        data: idCorte
      })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
