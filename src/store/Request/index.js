// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_REQUEST: [],
  S_REQUESTS: [],
  S_DATA: {}
}
const getters = {
  G_BUSINESS: (state) => {
    return state.S_BUSINESS.map((el) => ({
      name: el.nombre,
      value: String(el.idEmpresa)
    }))
  },
  G_REQUEST: (state) => (id) => {
    return state.S_REQUESTS.find(item => item.idSolicitud === id)
  },
  G_REQUESTS: (state) => {
    return state.S_REQUESTS.map((el) => ({
      name: el.nombre,
      value: String(el.idSolicitud)
    }))
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    console.log(params)
  },
  SET_REQUEST (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idSolicitud === params.data.idSolicitud)
    console.log('M_UPDATE_REQUEST', index)
  }
}
const actions = {
  async A_GET_REQUESTS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('solicitud/listar', { params })
      commit('SET_DATA', {
        destination: 'S_REQUESTS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_REQUEST [ACTION]', error)
      throw error
    }
  },
  async A_GET_REQUEST ({ commit }, { id, params }) {
    try {
      const { data } = await HTTP.get(`solicitud/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_REQUEST',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_REQUEST:', error)
      throw error
    }
  },
  async A_SET_REQUEST ({ commit }, solicitud) {
    try {
      const { data } = await HTTP.post('solicitud/agregar', { Solicitud: solicitud })

      commit('PUSH_DATA', {
        destination: 'S_REQUESTS',
        data: solicitud
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_REQUEST [ACTION]', error)
      throw error
    }
  },
  async A_UPDATE_REQUEST ({ commit }, solicitud) {
    try {
      const { data } = await HTTP.put('solicitud/actualizar', { solicitud })
      return data
    } catch (error) {
      console.log('ERROR_UPDATE_REQUEST [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
