// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PROFILE: null,
  S_AUTH: null
}
const getters = {
  isAuthenticated () {
    return !!Vue.$cookies.get('AUTH_TOKEN')
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  M_SET_USER (state, username) {
    state.user = username
  },
  M_LOGOUT (state) {
    state.user = null
    state.posts = null
  }
}
const actions = {
  async A_REGISTER ({ dispatch }, form) {
    try {
      await HTTP.post('register', form)
      await dispatch('A_LOGIN', {
        username: form.username,
        password: form.password
      })
    } catch (error) {
      console.log('ERROR_REGISTER [ACTION]', error)
    }
  },
  async A_LOGIN ({ commit }, User) {
    try {
      const { data } = await HTTP.post('usuario/autenticar', User)
      Vue.$cookies.set('AUTH_TOKEN', data.token)
      Vue.$cookies.set('AUTH_ID', data.idUsuario)
      Vue.$cookies.set('AUTH_DATA', data)

      commit('SET_DATA', {
        destination: 'S_AUTH',
        data: data.token
      })
      commit('SET_DATA', {
        destination: 'S_PROFILE',
        data: data
      })
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_LOGOUT ({ commit }) {
    const user = null
    Vue.$cookies.remove('AUTH_TOKEN')
    Vue.$cookies.remove('AUTH_ID')
    Vue.$cookies.remove('AUTH_DATA')
    commit('SET_DATA', {
      destination: 'S_AUTH',
      data: user
    })
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
