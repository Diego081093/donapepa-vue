// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_LOTES: [],
  S_LOTE: []
}
const getters = {
  G_LOTES: (state) => {
    const products = []
    state.S_LOTES.forEach(element => {
      products.push(
        {
          name: element.nombre,
          value: String(element.idProducto)
        }
      )
    })
    if (products.length === 0) {
      products.push({ name: 'Sin data', value: '0' })
    }
    return products
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idLote === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_LOTE', index)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idLote === params.data.idLote)
    console.log('M_UPDATE_LOTE', index)
  }
}
const actions = {
  async A_SET_LOTE ({ commit }, lote) {
    try {
      const { data } = await HTTP.post('lote/agregar', { lote })
      commit('PUSH_DATA', {
        destination: 'S_LOTES',
        data: lote
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_LOTE [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
