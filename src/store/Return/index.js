// IMPORT LIBRARIES
import Vue from 'vue'

import { HTTP } from '@/plugins/axios'

const state = {
  S_RETURNS: [],
  S_RETURN: []
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idProducto === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_RETURN', index)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idProducto === params.data.idProducto)
    console.log('M_UPDATE_RETURN', index)
  }
}
const actions = {
  async A_GET_RETURNS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('tablero-devuelto/listar', { params })
      commit('SET_DATA', {
        destination: 'S_RETURNS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_RETURNS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
