// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_DISTRICTS: []
}
const getters = {
  G_DISTRICTS: (state) => {
    const districts = [{ name: 'Selecciona el Distrito', value: '' }]
    state.S_DISTRICTS.forEach(element => {
      districts.push(
        {
          name: element.nombre,
          value: element.idDistrito + ''
        }
      )
    })
    return districts
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_DISTRICTS ({ commit }, idProvincia = 0) {
    try {
      const { data } = await HTTP.get('distrito/listar/' + idProvincia)

      commit('SET_DATA', {
        destination: 'S_DISTRICTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_DISTRICTS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
