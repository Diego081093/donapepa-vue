import Vue from 'vue'
import Vuex from 'vuex'
// IMPORT plugins
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// import of modules
import Profile from './Profile/index'
import Auth from './Auth/index'
import Category from './Category/index'
import Employee from './Employee/index'
import Product from './Product/index'
import Local from './Local/index'
import Position from './Position/index'
import Department from './Department/index'
import Province from './Province/index'
import Client from './Client/index'
import Sale from './Sale/index'
import Report from './Report/index'
import Campaign from './Campaign/index'
import Board from './Board/index'
import Cut from './Cut/index'
import Lote from './Lote/index'
import Request from './Request/index'
import Stock from './Stock/index'
import Transfer from './Transfer/index'
import Return from './Return/index'
import Discount from './Discount/index'
import District from './District/index'

import { HTTP } from '@/plugins/axios'

Vue.use(Vuex)

export default new Vuex.Store({
  actions: {
    async A_DESACTIVE ({ commit }, Params) {
      try {
        const { data } = await HTTP.put('desactivar', Params)
        return data
      } catch (error) {
        console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
        throw error
      }
    },
    async A_ACTIVE ({ commit }, Params) {
      try {
        const { data } = await HTTP.put('activar', Params)
        return data
      } catch (error) {
        console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
        throw error
      }
    }
  },
  modules: {
    Auth,
    Profile,
    Category,
    Employee,
    Product,
    Local,
    Position,
    Department,
    Province,
    Client,
    Sale,
    Report,
    Campaign,
    Board,
    Cut,
    Lote,
    Request,
    Stock,
    Transfer,
    Return,
    Discount,
    District
  }
})
/**
 * THIS FORMAT NAMED FUNCTIONS :
 * State      -> S_
 * Mutation   -> M_
 * Action     -> A_
 * Getter     -> G_
 *
 * FORMAT FUNCTION LOGICS
 * SET      -> setting data in state
 * PUSH     -> pushing data in array state
 *
 * DEFAULT MUTATIONS
 * SET_DATA     -> set data in element state
 * PUSH_DATA    -> push data in element state
 */
