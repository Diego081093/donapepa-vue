// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PRODUCTS: [],
  S_PRODUCT: [],
  S_BRANDS: []
}
const getters = {
  G_PRODUCTS: (state) => {
    const products = []
    state.S_PRODUCTS.forEach(element => {
      products.push(
        {
          name: element.nombre,
          value: String(element.idProducto)
        }
      )
    })
    if (products.length === 0) {
      products.push({ name: 'Seleccione', value: '0' })
    }
    return products
  },
  G_BRANDS: (state) => {
    const brands = []
    state.S_BRANDS.forEach(element => {
      brands.push(
        {
          name: element.marca,
          value: String(element.marca)
        }
      )
    })
    if (brands.length === 0) {
      brands.push({ name: 'Seleccione', value: '0' })
    }
    return brands
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idProducto === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_PRODUCT', index)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idProducto === params.data.idProducto)
    console.log('M_UPDATE_PRODUCT', index)
  }
}
const actions = {
  async A_GET_PRODUCTS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('producto/listar', { params })
      commit('SET_DATA', {
        destination: 'S_PRODUCTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_PRODUCTS [ACTION]', error)
      throw error
    }
  },
  async A_SET_PRODUCT ({ commit }, product) {
    try {
      const { data } = await HTTP.post('producto/agregar', product)
      data.data.habilitado = 1
      commit('PUSH_DATA', {
        destination: 'S_PRODUCTS',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_PRODUCT [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_PRODUCT ({ commit }, id) {
    try {
      const { data } = await HTTP.put('producto/eliminar', { producto: { idProducto: id } })

      commit('DELETE_DATA', {
        destination: 'S_PRODUCTS',
        data: id
      })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_UPDATE_PRODUCT ({ commit }, product) {
    try {
      const { data } = await HTTP.put('producto/actualizar', product)
      /* const response = {
        ...product,
        ...data.data
      } */
      return data.data
    } catch (error) {
      console.log('ERROR_UPDATE_PRODUCT [ACTION]', error)
      throw error
    }
  },
  async A_GET_PRODUCT ({ commit }, { id, params }) {
    try {
      console.log('params', params)
      const product = await HTTP.get(`producto/${id}`, { params })
      return product
    } catch (error) {
      console.log('ERROR_GET_PRODUCT [ACTION]', error)
      throw error
    }
  },
  async A_GET_BRANDS ({ commit }) {
    try {
      const { data } = await HTTP.get('getBrandProduct')
      commit('SET_DATA', {
        destination: 'S_BRANDS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_PRODUCT [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
