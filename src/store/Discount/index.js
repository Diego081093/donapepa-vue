// IMPORT LIBRARIES
import Vue from 'vue'

import { HTTP } from '@/plugins/axios'

const state = {
  S_DISCOUNTS: [],
  S_DISCOUNT: {}
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_DISCOUNTS ({ commit }, params) {
    try {
      const idLocal = Vue.$cookies.get('AUTH_DATA').local.map(el => el.idLocal).join(',')
      params = {
        ...params,
        idLocal
      }
      const { data } = await HTTP.get('descuento/listar', { params })
      commit('SET_DATA', {
        destination: 'S_DISCOUNTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_DISCOUNTS [ACTION]', error)
      throw error
    }
  },
  async A_SET_DISCOUNT ({ commit }, solicitud) {
    try {
      const data = await HTTP.post('venta/solicitardescuento', solicitud)
      return data
    } catch (error) {
      console.log('ERROR_SET_DISCOUNT [ACTION]', error)
      throw error
    }
  },
  async A_GET_DISCOUNT ({ commit }, { hash }) {
    try {
      const { data } = await HTTP.get(`venta/descuento?hash=${hash}`)
      commit('SET_DATA', {
        destination: 'S_DISCOUNT',
        data: data
      })
      return data
    } catch (error) {
      console.log('A_GET_DISCOUNT [ACTION]', error)
      throw error
    }
  },
  async A_CONFIRM_DISCOUNT ({ commit }, body) {
    try {
      const { data } = await HTTP.post('venta/validardescuento', body)
      return data
    } catch (error) {
      console.log('ERROR_CONFIRM_DISCTOUNT:', error)
      throw error
    }
  },
  async A_DELETE_DISCOUNT ({ commit }, body) {
    try {
      const { data } = await HTTP.post('descuento/eliminar', body)
      return data
    } catch (error) {
      console.log('ERROR_DELETE_DISCOUNT:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
