import Vue from 'vue'

const ShoppingCart = {
  namespaced: true,

  state: {
    data: []
  },

  getters: {
    resetData (state) {
      return state.data
    }
  },

  mutations: {
    SET_DATA (state, params) {
      // console.log(params)
      Vue.set(state, params.destination, params.data)
    },
    PUSH_DATA (state, params) {
      state[params.destination].push(params.data)
    }
  },

  actions: {
    set_data (context, params) {
      context.commit('SET_DATA', {
        destination: params.destination,
        data: params.data
      })
    }
  }
}
export default ShoppingCart
