// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_TRANSFERS: [],
  S_TRANSFER: {}
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    console.log(params)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idTransferencia === params.data.idTransferencia)
    console.log('M_UPDATE_TRANSFER', index)
  }
}
const actions = {
  async A_GET_TRANSFERS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('transferencia/listar', { params })
      commit('SET_DATA', {
        destination: 'S_TRANSFERS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_TRANSFER [ACTION]', error)
      throw error
    }
  },
  async A_GET_TRANSFER ({ commit }, { id, params }) {
    try {
      const { data } = await HTTP.get(`transferencia/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_TRANSFER',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_TRANSFER:', error)
      throw error
    }
  },
  async A_SET_TRANSFER ({ commit }, transferencia) {
    try {
      const { data } = await HTTP.post('transferencia/agregar', { transferencia })
      commit('PUSH_DATA', {
        destination: 'S_TRANSFERS',
        data: transferencia
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_TRANSFER [ACTION]', error)
      throw error
    }
  },
  async A_VIEW_TRANSFER ({ commit }, id) {
    try {
      const { data } = await HTTP.put(`transferencia/${id}/vistar`)
      return data
    } catch (error) {
      console.log('ERROR_VIEW_TRANSFER [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
