// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_BUSINESS: [],
  S_LOCAL: [],
  S_EMPLOYEES: [],
  S_LOCALS: [],
  S_DATA: {}
}
const getters = {
  G_BUSINESS: (state) => {
    return state.S_BUSINESS.map((el) => ({
      name: el.nombre,
      value: String(el.idEmpresa)
    }))
  },
  G_LOCAL: (state) => (id) => {
    console.log(state.S_LOCALS)
    return state.S_LOCALS.find(item => item.idLocal === id)
  },
  G_LOCALS: (state) => {
    return state.S_LOCALS.map((el) => ({
      name: el.nombre,
      value: String(el.idLocal)
    }))
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    console.log(params)
  },
  SET_LOCAL (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idLocal === params.data.idLocal)
    console.log('M_UPDATE_LOCAL', index)
  }
}
const actions = {
  async A_GET_BUSINESS ({ commit }) {
    try {
      const { data } = await HTTP.get('empresa/listar')
      commit('SET_DATA', {
        destination: 'S_BUSINESS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_BUSINESS:', error)
      throw error
    }
  },
  async A_GET_LOCAL ({ commit }, { idLocal, params }) {
    try {
      const { data } = await HTTP.get(`local/detalle/${idLocal}`, { params })
      commit('SET_DATA', {
        destination: 'S_LOCAL',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_LOCAL [ACTION]', error)
      throw error
    }
  },
  async A_GET_LOCALS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('local/listar', { params })
      commit('SET_DATA', {
        destination: 'S_LOCALS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_LOCAL [ACTION]', error)
      throw error
    }
  },
  async A_GET_EMPLOYEES ({ commit }) {
    try {
      const { data } = await HTTP.get('empleado/listar?exclude=1')

      commit('SET_DATA', {
        destination: 'S_EMPLOYEES',
        data: data
      })
    } catch (error) {
      console.log('ERROR_GET_EMPLOYEES [ACTION]', error)
      throw error
    }
  },
  async A_SET_LOCAL ({ commit }, local) {
    try {
      const { data } = await HTTP.post('local/agregar', local)
      local.local.idLocal = data.id
      local.local.regalo = local.local.present
      commit('PUSH_DATA', {
        destination: 'S_LOCALS',
        data: local.local
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_LOCAL [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_LOCAL ({ commit }, id) {
    try {
      const { data } = await HTTP.put('local/eliminar', { local: { idLocal: id } })
      commit('DELETE_DATA', {
        destination: 'S_LOCAL',
        data: id
      })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_UPDATE_LOCAL ({ commit }, local) {
    try {
      const { data } = await HTTP.put('local/actualizar', local)
      return data
    } catch (error) {
      console.log('ERROR_UPDATE_LOCAL [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
