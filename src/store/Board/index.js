// IMPORT LIBRARIES
import Vue from 'vue'

import { HTTP } from '@/plugins/axios'

const state = {
  S_BOARDS: [],
  S_TYPESBOARD: []
}
const getters = {
  G_TYPESBOARD: (state) => {
    return state.S_TYPESBOARD.map(element => ({
      name: element.nombre,
      value: String(element.idTipoTablero)
    }))
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idTablero === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_BOARD', index)
  }
}
const actions = {
  async A_GET_BOARDS ({ commit }) {
    try {
      const { data } = await HTTP.get('tablero/listar')
      commit('SET_DATA', {
        destination: 'S_BOARDS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_BOARDS [ACTION]', error)
      throw error
    }
  },
  async A_GET_TYPESBOARD ({ commit }) {
    try {
      const { data } = await HTTP.get('tipo-tablero/listar')
      commit('SET_DATA', {
        destination: 'S_TYPESBOARD',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_TYPESBOARD [ACTION]', error)
      throw error
    }
  },
  async A_SET_BOARD ({ commit }, board) {
    try {
      const { data } = await HTTP.post('tablero/agregar', { tablero: board })
      board.idTablero = data
      commit('PUSH_DATA', {
        destination: 'S_BOARDS',
        data: board
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_BOARD [ACTION]', error)
      throw error
    }
  },
  async A_GET_BOARD_RETURNED ({ commit }) {
    try {
      const { data } = await HTTP.get('tablero-devuelto/listar')
      commit('SET_DATA', {
        destination: 'S_BOARDS',
        data
      })
      return data
    } catch (error) {
      console.error('ERROR_GET_BOARD_RETURNED', error)
      throw error
    }
  },
  async A_SET_BOARD_RETURNED ({ commit }, board) {
    try {
      const { data } = await HTTP.post('tablero-devuelto/agregar', { tableroDevuelto: board })
      commit('PUSH_DATA', {
        destination: 'S_BOARDS',
        data: board
      })
      return data
    } catch (error) {
      console.error('ERROR_SET_BOARD_RETURNED', error)
      throw error
    }
  },
  async A_SET_BOARD__RETURNED_REMOVE ({ commit }, tableroDevuelto) {
    try {
      const { data } = await HTTP.put('tablero-devuelto/quitar', { tableroDevuelto })
      console.log(data)
      commit('DELETE_DATA', {
        destination: 'S_BOARDS',
        data: data.data.idTablero
      })
      return data
    } catch (error) {
      console.error('ERROR_SET_BOARD_RETURNED', error)
      throw error
    }
  },
  async A_GET_BOARD ({ commit }, idTablero) {
    try {
      const { data } = await HTTP.get(`tablero/${idTablero}`)
      return data
    } catch (error) {
      console.error('ERROR_GET_BOARD', error)
      throw error
    }
  },
  async A_UPDATE_BOARD ({ commit }, tablero) {
    try {
      const { data } = await HTTP.put('tablero/actualizarTablero', { tablero })
      return data
    } catch (error) {
      console.error('ERROR_UPDATE_BOARD', error)
      throw error
    }
  },
  async A_DELETE_BOARD ({ commit }, idTablero) {
    try {
      const { data } = await HTTP.put(`tablero/eliminar/${idTablero}`)
      return data
    } catch (error) {
      console.error('ERROR_GET_BOARD', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
