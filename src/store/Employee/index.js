// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_EMPLOYEES: [],
  S_EMPLOYEE: {},
  S_ASIGNATION: {}
}
const getters = {}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idEmpleado === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_EMPLOYEE', index)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idEmpleado === params.data.idEmpleado)
    console.log('M_UPDATE_EMPLOYEE', index)
  }
}
const actions = {
  async A_GET_EMPLOYEES ({ commit }, params) {
    try {
      const { data } = await HTTP.get('empleado/listar', { params })
      commit('SET_DATA', {
        destination: 'S_EMPLOYEES',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_EMPLOYEES [ACTION]', error)
      throw error
    }
  },
  async A_SET_EMPLOYEE ({ commit }, employee) {
    try {
      const { data } = await HTTP.post('empleado/agregar', employee)
      if (data.status) {
        commit('PUSH_DATA', {
          destination: 'S_EMPLOYEES',
          data: data.data
        })
      }
      return data
    } catch (error) {
      console.log('ERROR_SET_EMPLOYEE [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_EMPLOYEE ({ commit }, id) {
    try {
      const { data } = await HTTP.put('empleado/eliminar', { empleado: { idEmpleado: id } })

      commit('DELETE_DATA', {
        destination: 'S_EMPLOYEES',
        data: id
      })
      return data
    } catch (error) {
      console.log('ERROR_DELETE_EMPLOYEE [ACTION]', error)
      throw error
    }
  },
  async A_DESACTIVE_EMPLOYEE ({ commit }, id) {
    try {
      const { data } = await HTTP.put('desactivar', { desactivar: { nombreTabla: 'Empleado', id: id } })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_ACTIVE_EMPLOYEE ({ commit }, id) {
    try {
      const { data } = await HTTP.put('activar', { activar: { nombreTabla: 'Empleado', id: id } })
      return data
    } catch (error) {
      console.log(error)
      throw error
    }
  },
  async A_UPDATE_EMPLOYEE ({ commit }, employee) {
    try {
      const { data } = await HTTP.put('empleado/actualizar', employee)
      return data
    } catch (error) {
      console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
      throw error
    }
  },
  async A_SET_ASIGNATION ({ commit }, asignacion) {
    try {
      const { data } = await HTTP.post('asignacion/agregar', asignacion)
      commit('SET_DATA', {
        destination: 'S_ASIGNATION',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_ASIGNATION [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_ASIGNATION ({ commit }, asignacion) {
    try {
      const { data } = await HTTP.put('asignacion/eliminar', asignacion)

      commit('SET_DATA', {
        destination: 'S_ASIGNATION',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_ASIGNATION [ACTION]', error)
      throw error
    }
  },
  async A_SET_SUPERVISOR ({ commit }, asignacion) {
    try {
      const { data } = await HTTP.post('asignacion/supervisor', asignacion)

      commit('SET_DATA', {
        destination: 'S_ASIGNATION',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_ASIGNATION [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_SUPERVISOR ({ commit }, idLocal) {
    try {
      const { data } = await HTTP.delete('asignacion/supervisor/' + idLocal)

      commit('SET_DATA', {
        destination: 'S_ASIGNATION',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_UNASIGNATION [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
