// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PAGINATE: null,
  S_CLIENTS: [],
  S_CLIENT: [],
  S_CLIENTE: {},
  S_CLIENT_DETAIL: {}
}
const getters = {
  G_CLIENTS: (state) => {
    const asdas = []
    state.S_CLIENTS.forEach(element => {
      asdas.push(
        {
          name: element.nombre,
          value: element.idCliente + ''
        }
      )
    })
    return asdas
  },
  G_CLIENT: (state) => {
    const asdasa = []
    state.S_CLIENT.forEach(element => {
      asdasa.push(
        {
          name: element.nombre,
          value: element.idCliente + ''
        }
      )
    })
    return asdasa
  }
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_CLIENTS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('cliente/listar', { params })
      commit('SET_DATA', {
        destination: 'S_PAGINATE',
        data: data.totalPaginator
      })
      commit('SET_DATA', {
        destination: 'S_CLIENTS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CLIENTS [ACTION]', error)
      throw error
    }
  },
  async A_GET_CLIENT ({ commit }, dni) {
    try {
      const data = await HTTP.get(`cliente/${dni}`)
      commit('SET_DATA', {
        destination: 'S_CLIENT',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_GET_CLIENT [ACTION]', error)
      return {}
    }
  },
  async A_GET_CLIENT_DETAIL ({ commit }, { dni, params }) {
    try {
      const data = await HTTP.get(`cliente/detalleCompra/${dni}`, { params })
      commit('SET_DATA', {
        destination: 'S_CLIENT_DETAIL',
        data: data.data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_CLIENT [ACTION]', error)
      return {}
    }
  },
  async A_SET_CLIENT ({ commit }, client) {
    try {
      console.log('cliente')
      console.log(client)
      const { data } = await HTTP.post('cliente/agregar', client)

      commit('SET_DATA', {
        destination: 'S_CLIENTE',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_CATEGORIE [ACTION]', error)
      throw error
    }
  },
  async A_UPDATE_CLIENT ({ commit }, client) {
    try {
      const { data } = await HTTP.put('cliente/actualizar', client)
      commit('SET_DATA', {
        destination: 'S_CLIENTE',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_CATEGORIE [ACTION]', error)
      throw error
    }
  },
  async A_GET_CAMPAIGN_CLIENTS ({ commit }, id) {
    try {
      const { data } = await HTTP.get(`campana/cliente/${id}/listar`)

      commit('SET_DATA', {
        destination: 'S_CLIENTS',
        data: data
      })
    } catch (error) {
      console.log('ERROR_GET_CAMPAIGN_CLIENTS [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
