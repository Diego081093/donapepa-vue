// IMPORT LIBRARIES
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'
import Vue from 'vue'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_HISTORY: [],
  S_REQUEST: [],
  S_CONFIRM: {},
  S_STOCK: [],
  S_STOCKS: [],
  S_DATA: {},
  S_STOCKDEVOLUCION: {},
  S_STOCKDEVOLUCIONS: []
}
const getters = {
  G_STOCK: (state) => (id) => {
    return state.S_STOCKS.find(item => item.idStock === id)
  },
  G_STOCKS: (state) => {
    return state.S_STOCKS.map((el) => ({
      name: el.nombre,
      value: String(el.idStock)
    }))
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  SET_LOCAL (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  SET_STOCK (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idLocal === params.data.idLocal)
    console.log('M_UPDATE_LOCAL', index)
  }
}
const actions = {
  async A_GET_HISTORY ({ commit }, params) {
    try {
      const { data } = await HTTP.get('stock', { params })
      commit('SET_DATA', {
        destination: 'S_HISTORY',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_HISTORY:', error)
      throw error
    }
  },
  async A_GET_REQUEST ({ commit }, params) {
    try {
      const { data } = await HTTP.get('solicitud/listar', { params })
      commit('SET_DATA', {
        destination: 'S_REQUEST',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_REQUEST:', error)
      throw error
    }
  },
  async A_GET_STOCKS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('stock', { params })
      commit('SET_DATA', {
        destination: 'S_STOCKS',
        data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_STOCKS:', error)
      throw error
    }
  },
  async A_CONFIRM_STOCK ({ commit }, params) {
    try {
      const { data } = await HTTP.put('stock/confirm/' + params.idStock, { productos: params.productos })
      commit('SET_DATA', {
        destination: 'S_CONFIRM',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_S_CONFIRM [ACTION]', error)
      console.log('ERROR_GET_STOCKS:', error)
      throw error
    }
  },
  async A_SET_STOCK ({ commit }, stock) {
    try {
      const { data } = await HTTP.post('stock/agregar', { stock })

      commit('PUSH_DATA', {
        destination: 'S_STOCKS',
        data: stock
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_STOCK [ACTION]', error)
      throw error
    }
  },
  async A_SET_STOCKVERIFICATION ({ commit }, stockVerificacion) {
    try {
      const { data } = await HTTP.post('stockverificacion/agregar', { stockVerificacion })
      return data
    } catch (error) {
      console.log('ERROR_SET_STOCKVERIFICATION [ACTION]', error)
      throw error
    }
  },
  async A_GET_STOCKDEVOLUCIONS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('stockdevolucion/listar', { params })
      commit('SET_DATA', {
        destination: 'S_STOCKDEVOLUCIONS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_STOCKDEVOLUCION [ACTION]', error)
      throw error
    }
  },
  async A_GET_STOCKDEVOLUCION ({ commit }, { id, params }) {
    try {
      const { data } = await HTTP.get(`stockdevolucion/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_STOCKDEVOLUCION',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_STOCKDEVOLUCION [ACTION]', error)
      throw error
    }
  },
  async A_SET_STOCKDEVOLUCION ({ commit }, stockVerificacion) {
    try {
      const { data } = await HTTP.post('stockdevolucion/agregar', { stockVerificacion })
      return data
    } catch (error) {
      console.log('ERROR_SET_STOCKDEVOLUCION [ACTION]', error)
      throw error
    }
  },
  async A_ACCEPT_STOCKDEVOLUCION ({ commit }, body) {
    try {
      const { data } = await HTTP.post('stockdevolucionDetalle/aceptar', body)
      return data
    } catch (error) {
      console.log('ERROR_ACCEPT_STOCKDEVOLUCION', error)
      throw error
    }
  },
  async A_CLOSE_STOCKDEVOLUCION ({ commit }, { idStockVerificacion, body }) {
    try {
      const { data } = await HTTP.put(`stockdevolucion/${idStockVerificacion}/cerrar`, body)
      return data
    } catch (error) {
      console.log('ERROR_ACCEPT_STOCKDEVOLUCION', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
