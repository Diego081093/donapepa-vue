// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_PROVINCES: []
}
const getters = {
  G_PROVINCES: (state) => {
    const provinces = []
    state.S_PROVINCES.forEach(element => {
      provinces.push(
        {
          name: element.nombre,
          value: element.idProvincia + ''
        }
      )
    })
    if (provinces.length === 0) {
      provinces.push({ name: 'Sin data', value: '0' })
    }
    return provinces
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  }
}
const actions = {
  async A_GET_PROVINCES ({ commit }, idDepartment = 1) {
    try {
      const { data } = await HTTP.get('provincia/listar/' + idDepartment)

      commit('SET_DATA', {
        destination: 'S_PROVINCES',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_PROVINCES [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
