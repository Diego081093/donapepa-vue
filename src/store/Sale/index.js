// IMPORT LIBRARIES
import Vue from 'vue'

import { HTTP } from '@/plugins/axios'

const state = {
  S_SALES: [],
  S_LIQUIDATION_MASTER: []
}
const getters = {
  G_LAST_SALE: (state) => {
    return {
      numero: state.S_SALE.numero
    }
  }
}
const mutations = {
  SET_DATA (state, params) {
    Vue.set(state, params.destination, params.data)
  },
  DELETE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idDocumentoVenta === params.data)
    state[params.destination].splice(index, 1)
    console.log('M_DELETE_SALE', index)
  }
}
const actions = {
  async A_GET_LAST_SALE ({ commit }, { idLocal, tipoDocumento }) {
    try {
      const { data } = await HTTP.get(`venta/ultimaVenta/${idLocal}?tipoDocumento=${tipoDocumento}`)
      commit('SET_DATA', {
        destination: 'S_SALE',
        data: data
      })
      return data
    } catch (error) {
      console.log('A_GET_LAST_SALE [ACTION]', error)
      throw error
    }
  },
  async A_GET_SALES ({ commit }, params) {
    try {
      const id = Vue.$cookies.get('AUTH_ID')
      const { data } = await HTTP.get(`venta/listar/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_SALES',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_SALE [ACTION]', error)
      throw error
    }
  },
  async A_SET_SALE ({ commit }, sale) {
    try {
      sale.idUsuario = Vue.$cookies.get('AUTH_ID')
      console.log(sale)
      const data = await HTTP.post('venta/emitir', sale)
      return data
    } catch (error) {
      console.log('ERROR_SET_SALE [ACTION]', error)
      throw error
    }
  },
  async A_SET_ANULATE ({ commit }, body) {
    try {
      const data = await HTTP.put('venta/anular', body)
      commit('DELETE_DATA', {
        destination: 'S_SALES',
        data: body.documentoVenta.idDocumentoVenta
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_SALE [ACTION]', error)
      throw error
    }
  },
  async A_SET_LIQUIDATION ({ commit }, liquidation) {
    try {
      const data = await HTTP.put('venta/liquidardiario', liquidation)
      return data
    } catch (error) {
      console.log('ERROR_SET_LIQUIDATION [ACTION]', error)
      throw error
    }
  },
  async A_GET_LIQUIDATION_MASTER ({ commit }, params) {
    try {
      const id = Vue.$cookies.get('AUTH_ID')
      const { data } = await HTTP.get(`reporte/liquidacionmaestro/${id}`, { params })
      commit('SET_DATA', {
        destination: 'S_LIQUIDATION_MASTER',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_SALE [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
