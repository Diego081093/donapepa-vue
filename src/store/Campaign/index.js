// IMPORT LIBRARIES
import Vue from 'vue'
// import jwtDecode from 'jwt-decode'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_LASTS: [],
  S_CURRENTS: []
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  },
  UPDATE_DATA (state, params) {
    const index = state[params.destination].findIndex(item => item.idCampana === params.data.idCampana)
    state[params.destination][index] = params.data
  }
}
const actions = {
  async A_GET_LASTS ({ commit }, params) {
    try {
      const { data } = await HTTP.get('campana/listarPasada', { params })
      // console.log('jwt decode', jwtDecode(commit))

      commit('SET_DATA', {
        destination: 'S_LASTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_LASTS [ACTION]', error)
      throw error
    }
  },
  async A_GET_CURRENT ({ commit }, params) {
    try {
      const { data } = await HTTP.get('campana/listarVigente', { params })
      // console.log('jwt decode', jwtDecode(commit))

      commit('SET_DATA', {
        destination: 'S_CURRENTS',
        data: data
      })
      return data
    } catch (error) {
      console.log('ERROR_GET_CURRENT [ACTION]', error)
      throw error
    }
  },
  async A_SET_CAMPAIGN ({ commit }, campaign) {
    try {
      const { data } = await HTTP.post('campana/agregar', campaign)

      commit('PUSH_DATA', {
        destination: 'S_CURRENTS',
        data: campaign
      })
      return data
    } catch (error) {
      console.log('ERROR_SET_CAMPAIGN [ACTION]', error)
      throw error
    }
  },
  async A_UPDATE_CAMPAIGN ({ commit }, campaign) {
    try {
      const { data } = await HTTP.put('campana/actualizar', campaign)
      commit('UPDATE_DATA', {
        destination: 'S_CURRENTS',
        data: campaign.campana
      })
      return data
    } catch (error) {
      console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_CAMPAIGN ({ commit }, campaign) {
    try {
      const { data } = await HTTP.put('campana/eliminar', campaign)
      commit('UPDATE_DATA', {
        destination: 'S_CURRENTS',
        data: campaign.campana
      })
      return data
    } catch (error) {
      console.log('ERROR_UPDATE_EMPLOYEE [ACTION]', error)
      throw error
    }
  },
  async A_SET_PRODUCT_CAMPAIGN ({ commit }, body) {
    try {
      const response = await HTTP.post('campana_producto/agregar', { campanaProducto: body })
      const { data } = await HTTP.get('campana/listarVigente')
      // console.log('jwt decode', jwtDecode(commit))

      commit('SET_DATA', {
        destination: 'S_CURRENTS',
        data: data
      })
      return response
    } catch (error) {
      console.log('ERROR_SET_PRODUCT_CAMPAIGN [ACTION]', error)
      throw error
    }
  },
  async A_UPDATE_PRODUCT_CAMPAIGN ({ commit }, body) {
    try {
      const response = await HTTP.put('campana_producto/actualizar', { campanaProducto: body })
      const { data } = await HTTP.get('campana/listarVigente')
      // console.log('jwt decode', jwtDecode(commit))

      commit('SET_DATA', {
        destination: 'S_CURRENTS',
        data: data
      })
      return response
    } catch (error) {
      console.log('ERROR_UPDATE_PRODUCT_CAMPAIGN [ACTION]', error)
      throw error
    }
  },
  async A_DELETE_PRODUCT_CAMPAIGN ({ commit }, idCampanaProducto) {
    try {
      const response = await HTTP.put('campana_producto/eliminar', { campanaProducto: { idCampanaProducto } })
      return response
    } catch (error) {
      console.log('ERROR_DELETE_PRODUCT_CAMPAIGN [ACTION]', error)
      throw error
    }
  },
  async A_SET_CLIENT_CAMPAIGN ({ commit }, body) {
    try {
      const response = await HTTP.post(`campana/cliente/${body.id}/agregar`, { clientes: body.clients })
      const { data } = await HTTP.get('campana/listarVigente')
      // console.log('jwt decode', jwtDecode(commit))

      commit('SET_DATA', {
        destination: 'S_CURRENTS',
        data: data
      })
      return response
    } catch (error) {
      console.log('ERROR_SET_PRODUCT_CAMPAIGN [ACTION]', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
