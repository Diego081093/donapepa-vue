// IMPORTS OF PAGES

// CREATE ROUTES AND VALIDATIONS
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'LoginForm' }
  },
  {
    path: '/login',
    // IMPORT LAYOUT  ==> DEFAULT
    component: () => import('../components/06-layouts/default.vue'),
    children: [
      {
        path: '',
        name: 'LoginForm',
        component: () => import('../components/05-pages/login/default.vue'),
        meta: {
          guest: true,
          title: 'Ingresar'
        }
      }
    ]
  },
  {
    path: '/dashboard',
    // IMPORT LAYOUT  ==> DASHBOARD
    component: () => import('../components/06-layouts/dashboard.vue'),
    children: [
      {
        path: '',
        name: 'DashboardPage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Dashboard'
        }
      },
      {
        path: '/reportes',
        name: 'ReportPage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Reportes'
        }
      },
      {
        path: '/reportes/ventas',
        name: 'ReportSalePage',
        component: () => import('../components/05-pages/report/sale/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Reporte Ventas'
        }
      },
      {
        path: '/reportes/liquidacion',
        name: 'ReportLiquidationPage',
        component: () => import('../components/05-pages/report/liquidation/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Reporte Liquidacion'
        }
      },
      {
        path: '/reportes/liquidacion/:id',
        name: 'ReportLiquidationDetailPage',
        component: () => import('../components/05-pages/report/liquidation/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Reporte Liquidacion'
        }
      },
      /**
       * SALESWOMAN PAGE START
       */
      {
        path: '/vendedora',
        name: 'SaleswomanPage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Base'
        }
      },
      {
        path: '/vendedora/cliente',
        name: 'SaleswomanClientPage',
        component: () => import('../components/05-pages/saleswoman/client/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      {
        path: '/vendedora/cliente/crear',
        name: 'SaleswomanClientCreatePage',
        component: () => import('../components/05-pages/saleswoman/client/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      {
        path: '/vendedora/cliente/:dni',
        name: 'SaleswomanClientDetailPage',
        component: () => import('../components/05-pages/saleswoman/client/detail.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      {
        path: '/vendedora/cliente/edit/:dni',
        name: 'SaleswomanClientEditPage',
        component: () => import('../components/05-pages/saleswoman/client/edit.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      {
        path: '/vendedora/reporte',
        name: 'SaleswomanReportPage',
        component: () => import('../components/05-pages/saleswoman/report/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Reporte'
        }
      },
      {
        path: '/vendedora/nueva-nota',
        name: 'SaleswomanNotePage',
        component: () => import('../components/05-pages/saleswoman/sale/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Nueva Nota'
        }
      },
      {
        path: '/vendedora/anulacion',
        name: 'SaleswomanAnulationPage',
        component: () => import('../components/05-pages/saleswoman/anulation/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Anulación'
        }
      },
      {
        path: '/vendedora/liquidacion',
        name: 'SaleswomanLiquidationPage',
        component: () => import('../components/05-pages/saleswoman/liquidation/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Liquidación'
        }
      },
      {
        path: '/vendedora/inventario',
        name: 'SaleswomanStockTakingPage',
        component: () => import('../components/05-pages/saleswoman/stocktaking/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Inventario'
        }
      },
      {
        path: '/vendedora/inventario/solicitar',
        name: 'SaleswomanStockTakingRequestPage',
        component: () => import('../components/05-pages/saleswoman/stocktaking/request.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Envío de almacen'
        }
      },
      {
        path: '/vendedora/inventario/envio-de-almacen/transferencia-de-otro-local/:id',
        name: 'SaleswomanStockTakingTransferPage',
        component: () => import('../components/05-pages/saleswoman/stocktaking/transfer.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Envío de almacen'
        }
      },
      {
        path: '/vendedora/transferencia',
        name: 'SaleswomanTransferPage',
        component: () => import('../components/05-pages/saleswoman/transfer/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Transferencia'
        }
      },
      {
        path: '/vendedora/transferir',
        name: 'SaleswomanTransferCreatePage',
        component: () => import('../components/05-pages/saleswoman/transfer/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Crear Transferencia'
        }
      },
      {
        path: '/vendedora/devolucion',
        name: 'SaleswomanReturnPage',
        component: () => import('../components/05-pages/saleswoman/return/details.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Devolucion'
        }
      },
      /**
       * SALESWOMAN PAGE END
       */
      {
        path: '/base',
        name: 'BasePage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Base'
        }
      },
      /**
       * TABLEROS DE TURRON PAGE START
       */
      {
        path: '/tableros/recibido',
        name: 'BoardReceivedPage',
        component: () => import('../components/05-pages/board/received/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Recibido'
        }
      },
      {
        path: '/tableros/recibido/nuevo',
        name: 'BoardReceivedCreatePage',
        component: () => import('../components/05-pages/board/received/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Nuevo tablero'
        }
      },
      {
        path: '/tableros/recibido/:idBoard/editar',
        name: 'BoardReceivedEditPage',
        component: () => import('../components/05-pages/board/received/edit.vue'),
        meta: {
          requiresAuth: true,
          title: 'Nuevo tablero'
        }
      },
      {
        path: '/tableros/regresado',
        name: 'BoardReturnedPage',
        component: () => import('../components/05-pages/board/returned/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Regresado'
        }
      },
      {
        path: '/tableros/regresado/:id/nuevo',
        name: 'BoardReturnedCreatePage',
        component: () => import('../components/05-pages/board/returned/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Nuevo tablero'
        }
      },
      {
        path: '/tableros/corte',
        name: 'BoardCutPage',
        component: () => import('../components/05-pages/board/cut/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Cortes'
        }
      },
      {
        path: '/tableros/corte/nuevo',
        name: 'BoardCutCreatePage',
        component: () => import('../components/05-pages/board/cut/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Nuevo corte'
        }
      },
      {
        path: '/tableros/producto',
        name: 'BoardProductPage',
        component: () => import('../components/05-pages/board/product/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Productos'
        }
      },
      {
        path: '/tableros/solicitados',
        name: 'BoardRequestProductPage',
        component: () => import('../components/05-pages/board/request/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Envíos'
        }
      },
      {
        path: '/tableros/solicitados/:id',
        name: 'BoardAcceptRequestProductPage',
        component: () => import('../components/05-pages/board/request/accept.vue'),
        meta: {
          requiresAuth: true,
          title: 'Solicitado'
        }
      },
      {
        path: '/tableros/envios',
        name: 'BoardSendProductPage',
        component: () => import('../components/05-pages/board/product/send.vue'),
        meta: {
          requiresAuth: true,
          title: 'Envíos'
        }
      },
      {
        path: '/tableros/producto/registro',
        name: 'BoardProductRegisterPage',
        component: () => import('../components/05-pages/board/product/register.vue'),
        meta: {
          requiresAuth: true,
          title: 'Registar ingreso'
        }
      },
      {
        path: '/tableros/producto/asignar',
        name: 'BoardProductAssignPage',
        component: () => import('../components/05-pages/board/product/assign.vue'),
        meta: {
          requiresAuth: true,
          title: 'Registar ingreso'
        }
      },
      {
        path: '/tableros/devolucion',
        name: 'BoardReturnPage',
        component: () => import('../components/05-pages/board/return/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Devoluciones'
        }
      },
      {
        path: '/tableros/devolucion/:id',
        name: 'BoardReturnDetailsPage',
        component: () => import('../components/05-pages/board/return/details.vue'),
        meta: {
          requiresAuth: true,
          title: 'Devoluciones'
        }
      },
      {
        path: '/tableros/confirmacion',
        name: 'BoardConfirmPage',
        component: () => import('../components/05-pages/board/confirm/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Confirmaciones'
        }
      },
      {
        path: '/tableros/conformidad/local',
        name: 'BoardConfirmLocalPage',
        component: () => import('../components/05-pages/board/confirm/local.vue'),
        meta: {
          requiresAuth: true,
          title: 'Confirmaciones'
        }
      },
      /**
       * TABLEROS DE TURRON PAGE END
       */
      /**
       * MAINTENANCE PAGE START
       */
      {
        path: '/mantenimiento/categoria',
        name: 'CategoryPage',
        component: () => import('../components/05-pages/maintenance/category/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Categoria'
        }
      },
      {
        path: '/mantenimiento/cliente',
        name: 'ClientPage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Cliente'
        }
      },
      {
        path: '/mantenimiento/empleado',
        name: 'EmployeePage',
        component: () => import('../components/05-pages/maintenance/employee/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Empleado'
        }
      },
      {
        path: '/mantenimiento/local',
        name: 'LocalPage',
        component: () => import('../components/05-pages/maintenance/local/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Local'
        }
      },
      {
        path: '/mantenimiento/producto',
        name: 'ProductPage',
        component: () => import('../components/05-pages/maintenance/product/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Producto'
        }
      },
      {
        path: '/mantenimiento/asignacion',
        name: 'AssignmentPage',
        component: () => import('../components/05-pages/maintenance/assignation/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Asignación'
        }
      },
      {
        path: '/mantenimiento/asignacion/asignar',
        name: 'AssignmentAssignPage',
        component: () => import('../components/05-pages/maintenance/assignation/create.vue'),
        meta: {
          requiresAuth: true,
          title: 'Asignar Supervisor'
        }
      },
      {
        path: '/mantenimiento/asignacion/historial',
        name: 'AssignmentHistoryPage',
        component: () => import('../components/05-pages/maintenance/assignation/history.vue'),
        meta: {
          requiresAuth: true,
          title: 'Asignación Historial'
        }
      },
      /**
       * MAINTENANCE PAGE END
       */
      /**
       * CAMPAIGN PAGE START
       */
      {
        path: '/campana',
        name: 'CampaignPage',
        component: () => import('../components/05-pages/campaign/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Campaña Vigente'
        }
      },
      {
        path: '/campana/:id',
        name: 'CampaignDetails',
        component: () => import('../components/05-pages/campaign/details.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      {
        path: '/campana/:id/clients',
        name: 'CampaignClientsPage',
        component: () => import('../components/05-pages/campaign/client/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Vendedora Cliente'
        }
      },
      /**
       * CAMPAIGN PAGE END
       */
      {
        path: '/descuentos',
        name: 'DisscountsPage',
        component: () => import('../components/05-pages/supervisor/discount/default.vue'),
        meta: {
          requiresAuth: true,
          title: 'Campaña Vigente'
        }
      },
      {
        path: '/configuracion',
        name: 'ConfigurationPage',
        component: () => import('../components/05-pages/base.vue'),
        meta: {
          requiresAuth: true,
          title: 'Configuración'
        }
      }
    ]
  },
  {
    path: '/logout',
    name: 'Logout',
    redirect: { name: 'LoginForm' }
  }
]

export default routes
