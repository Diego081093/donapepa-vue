import axios from 'axios'

export const HTTP = axios.create({
  baseURL: 'https://apidona.globoazul.pe/index.php/api/'
  // baseURL: 'http://api.donapepa.pe/index.php/api/'
})
